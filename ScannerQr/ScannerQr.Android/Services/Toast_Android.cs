﻿using Android.Widget;
using ScannerQr.Droid.Services;
using ScannerQr.Services;

[assembly: Xamarin.Forms.Dependency(typeof(Toast_Android))]
namespace ScannerQr.Droid.Services
{
    public class Toast_Android: IToast
    {
        public void Show(string message)
        {
            Android.Widget.Toast.MakeText(Android.App.Application.Context, message, ToastLength.Long).Show();
        }
    }
}