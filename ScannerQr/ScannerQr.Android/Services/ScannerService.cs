﻿using System;
using System.Collections.Generic;
using Android.Widget;
using ScannerQr.Droid.Services;
using ScannerQr.Services;
using ScannerQr.Models;
using Symbol.XamarinEMDK;
using Symbol.XamarinEMDK.Barcode;

[assembly: Xamarin.Forms.Dependency(typeof(ScannerService))]
namespace ScannerQr.Droid.Services
{
    public class ScannerService : Java.Lang.Object, EMDKManager.IEMDKListener, IScannerService
    {
        private EMDKManager _emdkManager;
        private BarcodeManager _barcodeManager;
        private Scanner _scanner;

        public event EventHandler<OnBarcodeScannedEventArgs> OnBarcodeScanned;
        
        public ScannerService() { }
        
        public void OnClosed()
        {
            if (_emdkManager != null) {
                _emdkManager.Release();
                _emdkManager = null;
            }
        }

        public void OnOpened(EMDKManager emdkManager)
        {
            _emdkManager = emdkManager;
            InitScanner();
        }

        public void InitScanner()
        {
            try {
                if (_emdkManager == null)
                    return;

                if (_barcodeManager == null) {
                    _barcodeManager = (BarcodeManager)_emdkManager.GetInstance(EMDKManager.FEATURE_TYPE.Barcode);
                    _scanner = _barcodeManager.GetDevice(BarcodeManager.DeviceIdentifier.Default);

                    if (_scanner != null)
                    {
                        _scanner.Data += scanner_Data;
                        _scanner.Status += scanner_Status;

                        _scanner.Enable();

                        //Configuracion
                        ScannerConfig scannerConfig = _scanner.GetConfig();
                        scannerConfig.SkipOnUnsupported = ScannerConfig.SkipOnUnSupported.None;
                        scannerConfig.ScanParams.DecodeLEDFeedback = true;
                        scannerConfig.DecoderParams.Code11.Enabled = true;
                        scannerConfig.DecoderParams.Code39.Enabled = true;
                        scannerConfig.DecoderParams.Code93.Enabled = true;
                        scannerConfig.DecoderParams.Code128.Enabled = true;
                        _scanner.SetConfig(scannerConfig);
                    }
                    else
                    {
                        Toast.MakeText(Android.App.Application.Context, "No se pudo activar Scanner ", ToastLength.Long).Show();
                    }
                }
            } catch (Exception ex) {
                Toast.MakeText(Android.App.Application.Context, "Init: " + ex.Message, ToastLength.Short).Show();
            }
        }

        private void scanner_Status(object sender, Scanner.StatusEventArgs e)
        {
            StatusData.ScannerStates state = e.P0.State;
            if (state == StatusData.ScannerStates.Idle)
            {
                try
                {
                    if (_scanner.IsEnabled && !_scanner.IsReadPending)
                    {
                        _scanner.Read();
                    }
                }
                catch (Exception ex)
                {
                    Toast.MakeText(Android.App.Application.Context, "IsEnabled: " + ex.Message, ToastLength.Long).Show();
                }
            }
        }

        private void scanner_Data(object sender, Scanner.DataEventArgs e)
        {
            ScanDataCollection scanDataCollection = e.P0;

            if (scanDataCollection != null && scanDataCollection.Result == ScannerResults.Success)
            {
                IList<ScanDataCollection.ScanData> scanData = scanDataCollection.GetScanData();
                List<ScannedBarCodes> scannedBarCodes = new List<ScannedBarCodes>();

                foreach (var data in scanData)
                {
                    string barCode = data.Data;
                    string symbology = data.LabelType.Name();
                    scannedBarCodes.Add(new ScannedBarCodes(barCode, symbology));
                }

                this.OnBarcodeScanned?.Invoke(this, new OnBarcodeScannedEventArgs(scannedBarCodes));
            }
        }

        internal void DeinitScanner() {
            if (_emdkManager != null)
            {
                if (_scanner != null) {
                    try
                    {
                        _scanner.Data -= scanner_Data;
                        _scanner.Status -= scanner_Status;
                        _scanner.Disable();
                    }
                    catch (ScannerException ex) {
                        Toast.MakeText(Android.App.Application.Context, "DeInit: " + ex.Message, ToastLength.Long).Show();
                    }
                }

                if (_barcodeManager != null) {
                    _emdkManager.Release(EMDKManager.FEATURE_TYPE.Barcode);
                }
                _barcodeManager = null;
                _scanner = null;
            }
        }

        public void Destroy() {
            if (_emdkManager != null) {
                _emdkManager.Release();
                _emdkManager = null;
            }
        }
    }
}