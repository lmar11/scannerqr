﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using ScannerQr.Droid.Services;
using Symbol.XamarinEMDK;
using System.Net;
using Xamarin.Forms;

namespace ScannerQr.Droid
{
    [Activity(Label = "ScannerQr", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public ScannerService Scanner { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            Rg.Plugins.Popup.Popup.Init(this, savedInstanceState);

            ServicePointManager.ServerCertificateValidationCallback += (o, cert, chain, errors) => true;

            Scanner = DependencyService.Get<ScannerService>();
            EMDKManager.GetEMDKManager(Android.App.Application.Context, Scanner);
            App.Scanner = Scanner;

            LoadApplication(new App());
        }

        protected override void OnResume()
        {
            base.OnResume();
            Scanner.InitScanner();
        }

        protected override void OnPause()
        {
            base.OnPause();
            Scanner.DeinitScanner();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            Scanner.Destroy();
        }
    }
}