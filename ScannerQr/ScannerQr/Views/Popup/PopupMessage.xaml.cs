﻿using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using ScannerQr.Config;
using ScannerQr.Models;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace ScannerQr.Views.Popup
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupMessage : PopupPage
	{
        private string _message;
        public string Message { get { return _message; } set { _message = value; OnPropertyChanged(); } }

        public Color _statusColor;
        public Color StatusColor { get { return _statusColor; } set { _statusColor = value; OnPropertyChanged(); } }

        public PopupMessage(TypeMessage type, string msg)
        {
            InitializeComponent();

            BindingContext = this;

            Message = msg;

            if (type == TypeMessage.MESSAGE_INFO) { StatusColor = Color.FromHex(Global.COLOR_INFO); }
            else if (type == TypeMessage.MESSAGE_SUCCESS) { StatusColor = Color.FromHex(Global.COLOR_SUCCESS); }
            else { StatusColor = Color.FromHex(Global.COLOR_DANGER); }
        }

        public void Cancel(object o, EventArgs e)
        {
            PopupNavigation.Instance.PopAsync(true);
        }
    }
}