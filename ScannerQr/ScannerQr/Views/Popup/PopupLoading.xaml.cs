﻿using Rg.Plugins.Popup.Pages;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace ScannerQr.Views.Popup
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopupLoading : PopupPage
    {
        public PopupLoading()
        {
            InitializeComponent();
        }
    }
}