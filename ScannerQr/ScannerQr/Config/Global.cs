﻿namespace ScannerQr.Config
{
    public class Global
    {
        public const int APP_ID = 14;//*12 - DESARROLLO* *8 - PRODUCCION*

        public const int STATUS_OK = 1000;
        public static int SESSION_TIME = 60;//en minutos
        public const int API_VALIDITY_TIME = 26; //en minutos

        public const string API_USERNAME = "/*usuarioSuministro*/";
        public const string API_PASSWORD = "/*passSuministro*/";

        public const string AZURE_PATH_API = "https://apievaluappsider.azurewebsites.net/";

        public const string COLOR_MAIN = "#525658";
        public const string COLOR_INFO = "#1a73e8";
        public const string COLOR_SUCCESS = "#01c351";
        public const string COLOR_DANGER = "#FF0B40";
        public const string COLOR_WARNNING = "#FF0B40";

        public const string P_ACCESS_TOKEN = "AccessToken";
        public const string P_USERNAME = "Username";
        public const string P_PERFIL = "Perfil";
        public const string P_CODI_PERFIL = "CodiPerfil";
        public const string P_CODI_PERS = "CodiPers";
        public const string P_FICHA_SAP = "FichaSap";
        public const string P_IS_LOGGED = "IsLogged";
        public const string P_FECHA_HORA = "FechaHora";
        public const string P_SESSION_DATE = "SessionDate";
    }
}
