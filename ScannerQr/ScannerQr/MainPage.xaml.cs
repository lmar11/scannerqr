﻿using Rg.Plugins.Popup.Services;
using ScannerQr.ViewModels;
using ScannerQr.Views.Popup;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace ScannerQr
{
    public partial class MainPage : ContentPage
    {
        [Preserve(AllMembers = true)]
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new MainViewModel(App.Scanner);
        }

        private void Button_Clicked(object sender, System.EventArgs e)
        {
            PopupNavigation.Instance.PushAsync(new PopupMessage(Models.TypeMessage.MESSAGE_DANGER, "Mensaje de Alerta"));
        }
    }
}
