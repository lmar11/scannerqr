﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScannerQr.Models
{
    public class Response
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public string Data { get; set; }
        public string Exception { get; set; }
        public string PathFoto { get; set; }
        public string FullName { get; set; }
    }
}
