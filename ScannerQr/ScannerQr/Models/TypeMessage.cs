﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScannerQr.Models
{
    public enum TypeMessage
    {
        MESSAGE_INFO,
        MESSAGE_SUCCESS,
        MESSAGE_DANGER
    }
}
