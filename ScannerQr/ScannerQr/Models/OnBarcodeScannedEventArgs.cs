﻿using System;
using System.Collections.Generic;
using Xamarin.Forms.Internals;

namespace ScannerQr.Models
{
    [Preserve(AllMembers = true)]
    public class OnBarcodeScannedEventArgs
    {
        public IEnumerable<ScannedBarCodes> BarCodes { get; set; }

        public OnBarcodeScannedEventArgs(IEnumerable<ScannedBarCodes> barCodes)
        {
            BarCodes = barCodes;
        }
    }
}
