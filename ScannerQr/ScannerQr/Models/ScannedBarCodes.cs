﻿using Xamarin.Forms.Internals;

namespace ScannerQr.Models
{
    [Preserve(AllMembers = true)]
    public class ScannedBarCodes
    {
        public string Barcode { get; set; }
        public string Symbology { get; set; }

        public ScannedBarCodes(string barcode, string symbology)
        {
            Barcode = barcode;
            Symbology = symbology;
        }
    }
}
