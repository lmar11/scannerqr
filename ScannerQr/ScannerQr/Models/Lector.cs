﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScannerQr.Models
{
    public class Lector
    {
        public string DeviceID { get; set; }
        public string Token { get; set; }
        public string Identifier { get; set; }
        public string UserRed { get; set; }
    }
}
