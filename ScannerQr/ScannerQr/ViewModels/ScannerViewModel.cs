﻿using Rg.Plugins.Popup.Services;
using ScannerQr.Config;
using ScannerQr.Models;
using ScannerQr.Services;
using ScannerQr.Views.Popup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace ScannerQr.ViewModels
{
    [Preserve(AllMembers = true)]
    public class ScannerViewModel : BaseViewModel
    {
        private CancellationTokenSource _cts;

        private string _scannedBarcodeText;
        public string ScannedBarcodeText
        {
            get { return _scannedBarcodeText; }
            set { _scannedBarcodeText = value; OnPropertyChanged(nameof(ScannedBarcodeText)); }
        }

        public IScannerService _scannerService;

        public ScannerViewModel(IScannerService scannerService)
        {
            ScannedBarcodeText = App.Current.Properties.ContainsKey("Error") ? (string) App.Current.Properties["Error"] : string.Empty;
            _scannerService = scannerService;
            _scannerService.OnBarcodeScanned += _scannerService_OnBarcodeScanned;
        }

        private async void _scannerService_OnBarcodeScanned(object sender, OnBarcodeScannedEventArgs e)
        {
            try
            {
                var scannedBarcode = e?.BarCodes?.FirstOrDefault();
                if (scannedBarcode == null)
                {
                    await PopupNavigation.Instance.PushAsync(new PopupMessage(TypeMessage.MESSAGE_DANGER, "Error de lectura."));
                    return;
                }

                if (!App.API.IsConnected())
                {
                    ScannedBarcodeText = "Sin internet";
                    await PopupNavigation.Instance.PushAsync(new PopupMessage(TypeMessage.MESSAGE_DANGER, "Revisa tu conexión a Internet."));
                    return;
                }

                var barcodeScanned = scannedBarcode.Barcode;
                var symbology = scannedBarcode.Symbology;

                ScannedBarcodeText = barcodeScanned;

                var lector = new Lector { DeviceID = "0001", Token = "Token0001", Identifier = "ID_0001", UserRed = "nsalinas"};
                await SendIdentifier(lector);
            }
            catch (Exception ex)
            {
                App.Toast(ex.Message);
                App.Current.Properties["Error"] = ex.Message;
                await App.Current.SavePropertiesAsync();
            }
        }

        private async Task SendIdentifier(Lector lector) {
            if (_cts != null) _cts.Cancel();

            CancellationTokenSource newCTS = new CancellationTokenSource();
            _cts = newCTS;

            var loading = new PopupLoading();
            try
            {
                await PopupNavigation.Instance.PushAsync(loading);

                var response = await App.API.SendIdentifier(lector, _cts.Token);

                ScannedBarcodeText = response.FullName;
                App.Toast(response.FullName);

                //if (response.Status == Global.STATUS_OK)
                //{
                //    ScannedBarcodeText = response.FullName;
                //    App.Toast("Identificador enviado.");
                //}
                //else
                //{
                //    ScannedBarcodeText = "Error al enviar identificador";
                //    App.Toast("Error al enviar el identificador.");
                //}
            }
            catch (Exception ex)
            {
                ScannedBarcodeText = ex.Message;
                App.Toast(ex.Message);
            }
            finally
            {
                await PopupNavigation.Instance.RemovePageAsync(loading);
            }
        }
    }
}
