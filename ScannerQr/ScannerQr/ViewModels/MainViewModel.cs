﻿using ScannerQr.Services;

namespace ScannerQr.ViewModels
{
    public class MainViewModel: ScannerViewModel
    {
        public MainViewModel(IScannerService scannerService) : base(scannerService)
        {
            ScannedBarcodeText = "Esperando lectura...";
        }
    }
}
