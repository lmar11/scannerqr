﻿using ScannerQr.Config;
using ScannerQr.Services;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace ScannerQr
{
    [Preserve(AllMembers = true)]
    public partial class App : Application
    {
        public static IScannerService Scanner { get; set; }
        public static App Current;
        public static ApiService API;
        public static string AccessToken;
        public static DateTime FechaHora;

        public App()
        {
            InitializeComponent();

            API = new ApiService();
            Current = this;

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            AccessToken = Properties.ContainsKey(Global.P_ACCESS_TOKEN) ? (string)Properties[Global.P_ACCESS_TOKEN] : string.Empty;
            FechaHora = Properties.ContainsKey(Global.P_FECHA_HORA) ? (DateTime)Properties[Global.P_FECHA_HORA] : DateTime.MinValue;
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public static void Toast(string message)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                DependencyService.Get<IToast>().Show(message);
            });
        }
    }
}
