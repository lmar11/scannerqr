﻿using Newtonsoft.Json;
using ScannerQr.Config;
using ScannerQr.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace ScannerQr.Services
{
    [Preserve(AllMembers = true)]
    public class ApiService
    {
        private const string url_auth = Global.AZURE_PATH_API + "api/login/authenticate";
        private const string url_login = Global.AZURE_PATH_API + "api/login/LoginUsuario";

        private const string url_send_identifier = Global.AZURE_PATH_API + "api/List/GetInfoTrabajador";//SendIdentifier
        private const string url_find_identifier = Global.AZURE_PATH_API + "api/List/GetAzurePaths";//api/List/FindIdentifier

        public bool IsConnected()
        {
            var state = Connectivity.NetworkAccess;
            if (state == NetworkAccess.None || state == NetworkAccess.Unknown)
            {
                return false;
            }
            return true;
        }

        public async Task ValidarToken()
        {
            try
            {
                var now = DateTime.Now - App.FechaHora;
                if (now.TotalMinutes >= Global.API_VALIDITY_TIME)//¿Token vencido?
                {
                    var token = await GetAccessToken();
                    if (!string.IsNullOrEmpty(token))
                    {
                        App.AccessToken = token;
                        App.FechaHora = DateTime.Now;

                        App.Current.Properties[Global.P_ACCESS_TOKEN] = App.AccessToken;
                        App.Current.Properties[Global.P_FECHA_HORA] = App.FechaHora;
                        await App.Current.SavePropertiesAsync();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public async Task<string> GetAccessToken()
        {
            try
            {
                HttpClient _client = new HttpClient(new HttpClientHandler());

                var param = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("Username", Global.API_USERNAME),
                    new KeyValuePair<string, string>("Password", Global.API_PASSWORD)
                };

                var request = new HttpRequestMessage(HttpMethod.Post, url_auth)
                {
                    Content = new FormUrlEncodedContent(param)
                };

                var response = await _client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    return data.Replace("\"", "");
                }
                else
                {
                    //Error en la API
                    throw new Exception();
                }
            }
            catch
            {
                throw;
            }
        }

        public async Task<Response> SendIdentifier(Lector model, CancellationToken ct)
        {
            try
            {
                await ValidarToken();

                HttpClient _client = new HttpClient(new HttpClientHandler());
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(App.AccessToken);

                var json = JsonConvert.SerializeObject(model);
                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                
                var response = await _client.PostAsync(url_send_identifier, httpContent, ct);
                
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    ct.ThrowIfCancellationRequested();
                    return JsonConvert.DeserializeObject<Response>(content);
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                throw;
            }
        }

        public async Task<Response> FindIdentifier(Lector model, CancellationToken ct)
        {
            try
            {
                await ValidarToken();

                HttpClient _client = new HttpClient(new HttpClientHandler());
                _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(App.AccessToken);

                var json = JsonConvert.SerializeObject(model);
                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _client.PostAsync(url_find_identifier, httpContent, ct);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    ct.ThrowIfCancellationRequested();
                    return JsonConvert.DeserializeObject<Response>(content);
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                throw;
            }
        }

    }
}
