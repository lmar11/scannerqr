﻿namespace ScannerQr.Services
{
    public interface IToast
    {
        void Show(string message);
    }
}
