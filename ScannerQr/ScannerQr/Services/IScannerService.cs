﻿using ScannerQr.Models;
using System;
using Xamarin.Forms.Internals;

namespace ScannerQr.Services
{
    [Preserve(AllMembers = true)]
    public interface IScannerService
    {
        event EventHandler<OnBarcodeScannedEventArgs> OnBarcodeScanned;
    }
}
